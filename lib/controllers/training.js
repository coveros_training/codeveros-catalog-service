const Training = require('../models/Training');

exports.getTraining = async ctx => {
  ctx.body = await Training.find();
};

exports.getOne = async ctx => {
  const id = ctx.params.id;
  const training = await Training.findById(id);

  if (!training) {
    ctx.throw(404, 'Training Class not found');
  }
  ctx.body = training;
};

exports.createTraining = async ctx => {
  let values = ctx.request.body;
  let newTraining = await Training.create(values);

  if (!newTraining || !newTraining._id) {
    ctx.throw(500, 'Error creating training');
  }
  ctx.body = newTraining;
};

exports.updateTraining = async ctx => {
  const id = ctx.params.id;
  const values = ctx.request.body;

  let foundTraining = await Training.findById(id);

  if (!foundTraining || !foundTraining._id) {
    ctx.throw(404, 'Training not found');
  }

  let updated = await Training.findByIdAndUpdate(id, values, { new: true });

  if (!updated || !updated._id) {
    ctx.throw(500, 'Error updating item');
  }

  ctx.body = updated;
};

exports.deleteTraining = async ctx => {
  const id = ctx.params.id;

  const training = await Training.findById(id);
  if (!training) {
    ctx.throw(404, 'Training Class not found');
  }

  let deletedTraining = await Training.findByIdAndRemove(id);

  if (!deletedTraining) {
    ctx.throw(500, 'Error deleting training');
  }

  ctx.body = deletedTraining;
};
